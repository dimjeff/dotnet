﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01ArrayContains
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Concatenate(new int[]{ 1,2,3},new int[]{ 5,7});
            Console.WriteLine(string.Join(",", arr));

            Console.WriteLine("any key....");
            Console.ReadKey();
        }

        public static int[] Concatenate(int[] a1, int[] a2) {
            int[] arr = new int[a1.Length+a2.Length];
            a1.CopyTo(arr, 0);
            a2.CopyTo(arr, a1.Length);
            return arr;
        }
    }
}
