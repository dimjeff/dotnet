﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{
    class City
    {
        public string Name;
        public double PopulationMillions;

        public override string ToString()
        {
            return string.Format("City {0} with {1:N2}% mil. population", Name,PopulationMillions);
        }
    }

    class BetterCity
    {
        public BetterCity(string name)
        {
            this.Name = name;
        }
        string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new InvalidOperationException("Name must be 2 chars or more");
                }
                _name = value;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BetterCity c2 = new BetterCity("Mp");
                Console.WriteLine("City is {0}", c2.Name);
            }catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }


            //City c1 = new City { Name = "Montreal", PopulationMillions = 3.567 };
            //Console.WriteLine(c1);

            Console.WriteLine("press any key....");
            Console.ReadKey();
        }
    }
}
