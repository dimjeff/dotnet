﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Names
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> nameList = new List<string>();
            while (true)
            {
                Console.Write("please input name:");
                string name = Console.ReadLine();
                if (name == "")
                    break;
                else
                    nameList.Add(name);
            }
            Console.WriteLine("[{0}]", string.Join(", ", nameList.ToArray()));

            Console.WriteLine("Press any key....");
            Console.ReadKey();
        }
    }
}
