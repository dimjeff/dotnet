﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01NamesAgain
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nameList = new string[5];
            int count = 0;
            while (count < nameList.Length)
            {
                Console.Write("Please input a name:");
                string name = Console.ReadLine();
                nameList[count] = name;
                count++;
            }
            Console.Write("please input search word:");
            string search = Console.ReadLine();

            var results = Array.FindAll(nameList, s => s.Contains(search));
            for(int i=0;i<results.Length;i++)
                Console.Write(string.Format("Matching name: {0}\n", results[i]));

            string longest = nameList.OrderByDescending(s => s.Length).First();
            Console.Write(string.Format("The longest name is {0}", longest));

            Console.ReadKey();
        }
    }
}
