﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numList = new List<int>();
            while (true)
            {
                Console.Write("How many numbers you want generate?");
                if (!int.TryParse(Console.ReadLine(), out int num))
                {
                    Console.WriteLine("Invalid number\n");
                    continue;
                }
                Random random = new Random();
                for (int i = 0; i < num; i++)
                {
                    int n = random.Next(-100, 100);
                    numList.Add(n);
                }
                Console.WriteLine(string.Join(", ", numList.ToArray()));

                List<int> resultNum = numList.FindAll(x => x <=0);
                Console.WriteLine(string.Join("\n", resultNum.ToArray()));

                Console.WriteLine("press any key....");
                Console.ReadKey();
                break;
            }
        }
    }
}
