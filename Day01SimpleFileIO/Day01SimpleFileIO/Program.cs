﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day01SimpleFileIO
{
    class Program
    {
        const string filename = @"..\..\output.txt";
        static void Main(string[] args)
        {
            using (StreamWriter ws = File.AppendText(filename))
            {
                ws.WriteLine("line wirting...");
            }

            string[] linesArray = { "Jerry", "Berry", "Marry" };
            File.AppendAllLines(filename, linesArray);

            string[] dataArray = File.ReadAllLines(filename);

            string alltext = File.ReadAllText(filename);
        }
    }
}
