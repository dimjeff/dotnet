﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            
            while (true)
            {
                Console.Write("How big does your tree needs to be?");
                if (!int.TryParse(Console.ReadLine(), out int num))
                {
                    continue;
                }

                for(int i = 1; i <= num; i++)
                {
                    //2n-1
                    for (int j=0;j<num - i;j++)
                    {
                        Console.Write(" ");
                    }
                    for(int j=0;j<2*i-1;j++)
                        Console.Write("*");
                    Console.Write("\n");
                }

                Console.WriteLine("any key....");
                Console.ReadKey();
                break;
            }
        }
    }
}
