﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day02PeopleAgain
{
    class Program
    {
        const string filename = @"..\..\people.txt";
        static List<Person> peopleList = new List<Person>();
        

        static void Main(string[] args)
        {
            int choice = getMenuChoice();

            switch (choice)
            {
                case 1:
                    Person.LogFailSet = PrintInfoToScreen;
                    break;
                case 2:
                    Person.LogFailSet = PrintInfoToScreen;
                    Person.LogFailSet += WriteInfoToFile;
                    break;
                case 3:
                    break;
                default:
                    Person.LogFailSet = PrintInfoToScreen;
                    break;
            }

            ReadDataFromFile();

            Console.WriteLine("");
/*            for (int i = 0; i < peopleList.Count; i++)
            {
                Console.WriteLine(peopleList[i]);
            }

            Console.WriteLine("");
            for (int i = 0; i < peopleList.Count; i++)
            {
                if (peopleList[i] is Student)
                    Console.WriteLine(peopleList[i]);
            }

            Console.WriteLine("");
            List<Person> list = peopleList.Where(a => a.GetType().Name == "Person").ToList();
            for (int i = 0; i < list.Count; i++)
            {
                //if (peopleList[i].GetType().Name == "Person")
                Console.WriteLine(list[i]);
            }*/

            // only students
            var onlyStudents = from p in peopleList where p is Student select (Student)p;
            // ordering people by name
            var peopleOrderedByName = from p in peopleList orderby p.Name select p;
            //
            var peopleNamesAgesInOrder = from p in peopleList orderby p.Dob select p.Name;
            //
            var peopleUnderage = from p in peopleList where p.Dob < DateTime.Parse("2000-01-01") select p;
            //
            var peopleUnderageAsTuples = from p in peopleList where p.Dob < new DateTime() select new Tuple<string, DateTime>(p.Name, p.Dob);
            //
            //var sumOfAges = (from p in peopleList select p.Age).Sum();
            //
            //var AverageOfAges = (from p in peopleList select p.Age).Average(); // make sure double is returned
            //order by case
            var peopleInOrderType = from p in peopleList orderby (p.GetType().Name == "Person" ? 1 : (p.GetType().Name == "Teacher" ? 2 : 3)) select p;
            //(p.GetType().Name== "Student"? "Z": p.GetType().Name) select p;
            //.OrderBy(p => (p.IsQualityNetwork == 1 || p.IsEmployee == 1) ? 0 : 1)
            //
/*            List<string> linesToWrite = new List<string>();
            foreach (Person p in peopleInOrderType)
            {
                Console.WriteLine(p);
            }*/
            // TODO: Write lines to file

            // collection to list
            List<Person> listOfPeopleOrderedByName = new List<Person>(peopleOrderedByName);

            //SaveDatatoFile();

            int[,] a1 = { { 1, 3 }, { 2, 3 } };
            int[,] a2 = { { 4, 3 }, { 5, 6 } };
            PrintDups(a1,a2);

            Console.ReadKey();
        }

        public static void PrintDups(int[,] a1, int[,] a2) {
            for (int i = 0; i < a1.GetLength(0); i++)
            {
                for (int j = 0; j < a1.GetLength(1); j++)
                {
                    Console.WriteLine(a1[i, j]);
                }
            }
            Console.WriteLine("");
            foreach (int i in a1)
            {
                foreach (var row in a2)
                    if (row==i)
                        Console.WriteLine(i);
            }
        }

        static int getMenuChoice()
        {
            int menu;
            try
            {
                Console.Write("1 - screen only\n"+
                                "2 - screen and file\n" +
                                "3 - do not log\n" +
                                "Your choice: ");
                //string strMenu = Console.ReadLine();
                menu = int.Parse(Console.ReadLine());

            }
            catch (FormatException ex)
            {
                menu = -1;
            }
            return menu;
        }

        static void PrintInfoToScreen(string reason)
        {
            Console.WriteLine("Event Object Creation: " + reason);
        }

        static void WriteInfoToFile(string reason)
        {
            try
            {
                File.AppendAllText(@"..\..\eventlog.txt", "ObjCreated: " + reason + "\r\n");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

        static void ReadDataFromFile()
        {
            try
            {
                string[] people = File.ReadAllLines(filename);
                foreach (string line in people)
                {
                    try
                    {
                        string[] arr = line.Split(';');
                        string type = arr[0];
                        string dataline = line.Substring(line.IndexOf(';') + 1, line.Length - line.IndexOf(';') - 1);
                        switch (type)
                        {
                            case "Person":
                                Person p = new Person(dataline);
                                peopleList.Add(p);
                                break;
                            case "Teacher":
                                Teacher t = new Teacher(dataline);
                                peopleList.Add(t);
                                break;
                            case "Student":
                                Student s = new Student(dataline);
                                peopleList.Add(s);
                                break;
                            default:
                                Console.WriteLine("Invalid data type:" + line);
                                break;
                        }
                    }
                    catch (InvalidParameterException ex)
                    {
                        //Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }

        static void SaveDatatoFile()
        {
            double t = (from p in peopleList where p is Student select ((Student)p).GPA).Average();
            //x => new { FieldName = x.Property }
            var list = peopleList.OrderBy(a => a.Name).ThenBy(n => n.Dob).ToList();
            //peopleList.Sort((p1, p2) => p1.Name.CompareTo(p2.Name));
            string[] text = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                text[i] = list[i].ToDataString();
                //Console.WriteLine(peopleList[i]);
            }
            File.WriteAllLines(filename, text);
        }
    }


}
