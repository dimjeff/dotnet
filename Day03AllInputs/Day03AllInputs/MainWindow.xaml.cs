﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (comboContinent.SelectedIndex == -1)
            {
                MessageBox.Show(this,"Continent is empty!","Input error");
                comboContinent.Focus();
                comboContinent.IsDropDownOpen = true;
                return;
            }
            string continent = comboContinent.Text;
            string name = tfName.Text;
            string age = "";
            if(age18To35.IsChecked==true)
                age = age18To35.Content.ToString();
            if (age36AndUp.IsChecked == true)
                age = age36AndUp.Content.ToString();
            if (ageBelow18.IsChecked == true)
                age = ageBelow18.Content.ToString();
            List<string> pets = new List<string>();
            if (petDog.IsChecked == true)
                pets.Add(petDog.Content.ToString());
            if (petCat.IsChecked == true)
                pets.Add(petCat.Content.ToString());
            if (petOther.IsChecked == true)
                pets.Add(petOther.Content.ToString());
            string strPets = string.Join(",", pets.ToArray());
            string temp = lblTemp.Text;

            lblResult.Content = string.Format("name {0},age {1},pets {2}, continent {3}, temp {4}", name, age, strPets, continent, temp);
        }

    }
}
