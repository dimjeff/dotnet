﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tfInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            calculateTemp();
        }

        private void calculateTemp()
        {
            
            string inputType = "";
            string outputType = "";

            ChildControls ccChildren = new ChildControls();

            foreach (object o in ccChildren.GetChildren(InputGroup, 1))
            {
                if (o.GetType() == typeof(RadioButton))
                {
                    RadioButton radio = (RadioButton)o;
                    if (radio.IsChecked == true)
                        inputType = radio.Content.ToString();
                }
            }
            foreach (object o in ccChildren.GetChildren(OutputGroup, 1))
            {
                if (o.GetType() == typeof(RadioButton))
                {
                    RadioButton radio = (RadioButton)o;
                    if (radio.IsChecked == true)
                        outputType = radio.Content.ToString();
                }
            }

            Regex rgx = new Regex("^[-+]?[0-9]*\\.?[0-9]+$");
            if (!rgx.IsMatch(tfInput.Text))
            {
                //MessageBox.Show("Wrong input data!", "Input error");
                return;
            }
            if (!double.TryParse(tfInput.Text, out double inputValue))
            {
                //MessageBox.Show("Wrong input data!", "Input error");
                return;
            }


            double celVal;
            switch (inputType.ToLower())
            {
                case "celcius":
                    celVal = inputValue;
                    break;
                    case "fahrenheit":
                    celVal = (inputValue - 32) * 5 / 9;
                    break;
                case "kelvin":
                    celVal = inputValue - 273.15;
                    break;
                default:
                    //MessageBox.Show("Internal error: unkown input scale!", "Input error");
                    return;
            }
            string resultVal;
            switch (outputType.ToLower())
            {
                case "celcius":
                    resultVal = celVal.ToString("0.##") + "°C";
                    break;
                    case "fahrenheit":
                    resultVal = (celVal * 9 / 5 + 32).ToString("0.##") + "°F";
                    break;
                case "kelvin":
                    resultVal = (celVal + 273.15).ToString("0.##") + "°K";
                    break;
                default:
                    //MessageBox.Show("Internal error: unkown output scale!", "Input error");
                    return;
            }
            tfOutput.Text = resultVal;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            calculateTemp();
        }
    }

    class ChildControls
    {
        private List<object> lstChildren;

        public List<object> GetChildren(Visual p_vParent, int p_nLevel)
        {
            if (p_vParent == null)
            {
                throw new ArgumentNullException("Element {0} is null!", p_vParent.ToString());
            }

            this.lstChildren = new List<object>();

            this.GetChildControls(p_vParent, p_nLevel);

            return this.lstChildren;

        }

        private void GetChildControls(Visual p_vParent, int p_nLevel)
        {
            int nChildCount = VisualTreeHelper.GetChildrenCount(p_vParent);

            for (int i = 0; i <= nChildCount - 1; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(p_vParent, i);

                lstChildren.Add((object)v);

                if (VisualTreeHelper.GetChildrenCount(v) > 0)
                {
                    GetChildControls(v, p_nLevel + 1);
                }
            }
        }
    }
}
