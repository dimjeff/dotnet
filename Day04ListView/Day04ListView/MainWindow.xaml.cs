﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Day04ListView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Person> myList;
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;
        const string filename = @"..\..\data.txt";
        Database db;

        public MainWindow()
        {
            InitializeComponent();
            db = new Database();
            myList = db.ReadData();
            //myList = new ObservableCollection<Person>();
            lvDataBinding.ItemsSource = myList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvDataBinding.ItemsSource);
            //view.SortDescriptions.Add(new SortDescription("Age", ListSortDirection.Descending));
            view.Filter = UserFilter;
            //ReadDataFromFile();
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFilter.Text))
                return true;
            else
                return ((item as Person).Name.IndexOf(txtFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void txtFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvDataBinding.ItemsSource).Refresh();
        }

        private void ReadDataFromFile()
        {
            try
            {
                string[] people = File.ReadAllLines(filename);
                foreach (string line in people)
                {
                    try
                    {
                        Person ap = new Person(int.Parse(line.Split(';')[0]), line.Split(';')[1],int.Parse(line.Split(';')[2]));
                        myList.Add(ap);
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
        } // file not found is not an error

        private void lvUsersColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvDataBinding.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvDataBinding.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }

        private void Age_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string name = tfName.Text;
            if (name == "")
            {
                MessageBox.Show("Name must not be empty!", "Input error");
                return;
            }

            if (!int.TryParse(tfAge.Text, out int age))
            {
                MessageBox.Show("Age must be 0-150!", "Input error");
                return;
            }

            Person p = new Person(0,name, age);
            myList.Add(p);
            tfName.Text = "";
            tfAge.Text = "";
            btImage.Source = null;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if(selectedPerson == null)
            {
                return;
            }

            string name = tfName.Text;
            if (name == "")
            {
                MessageBox.Show("Name must not be empty!", "Input error");
                return;
            }

            if (!int.TryParse(tfAge.Text, out int age))
            {
                MessageBox.Show("Age must be 0-150!", "Input error");
                return;
            }

            byte[] bytes = null;
            var bitmapSource = btImage.Source as BitmapSource;
            if (bitmapSource != null)
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            selectedPerson.Name = name;
            selectedPerson.Age = age;
            selectedPerson.ImageData = bytes;
            db.UPdateData(selectedPerson);
            lvDataBinding.Items.Refresh();
            MessageBox.Show(this, "It's done!", "Finished", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void DelButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPerson == null)
            {
                return;
            }
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete "+ selectedPerson, "Delete Confirmation", MessageBoxButton.OKCancel,MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.OK)
            {
                //Person p = (Person)lvDataBinding.SelectedValue;
                myList.Remove(selectedPerson);
                lvDataBinding.Items.Refresh();
            }
        }

        Person selectedPerson;

        private void lvDataBinding_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                btAdd.IsEnabled = false;
                btUpdate.IsEnabled = true;
                btDelete.IsEnabled = true;
                selectedPerson = (Person)lvDataBinding.SelectedItem;//but listview is multiple selection
                tfName.Text = selectedPerson.Name;
                tfAge.Text = "" + selectedPerson.Age;
                if (selectedPerson.ImageData != null)
                    btImage.Source = Database.ByteToImage(selectedPerson.ImageData);
                else
                    btImage.Source = null;
            }
            else
            {
                selectedPerson = null;
                tfName.Text = "";
                tfAge.Text = "0";
                btImage.Source = null;
                btAdd.IsEnabled = true;
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
            }

            //Person p = (Person)lvDataBinding.SelectedItems[0];
            //System.Windows.MessageBox.Show(p.Name);
        }

        private void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Filter = "Image Files|*.jpg;*.png;*.bmp;*.gif";
            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = openFileDlg.ShowDialog();
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                btImage.Source = new BitmapImage(new Uri(openFileDlg.FileName));
            }
        }
    }

    public class SortAdorner : Adorner
    {
        private static Geometry ascGeometry =
            Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private static Geometry descGeometry =
            Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                (
                    AdornedElement.RenderSize.Width - 15,
                    (AdornedElement.RenderSize.Height - 5) / 2
                );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }

    public class Person
    {
        int _no;
        public int MemberNo
        {
            get
            {
                return _no;
            }
            set
            {
                _no = value;
            }
        }

        string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public Person(int no,string name, int age)
        {
            MemberNo = no;
            Name = name;
            Age = age;
        }

        public Person(int no, string name, int age, byte[] image)
        {
            MemberNo = no;
            Name = name;
            Age = age;
            ImageData = image;
        }

        int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        byte[] _imageData;
        public byte[] ImageData
        {
            get { return this._imageData; }
            set { this._imageData = value; }
        }
        // TODO: define toString and constructor

        public override string ToString()
        {
            return string.Format("{0} is {1} y/o.", Name, Age);
        }


    }

    public class Database
    {
        protected readonly SqlConnection conn;

        public static ImageSource ByteToImage(byte[] imageData)
        {
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch(NotSupportedException ex)
            {
                return null;
            }
        }

        public static Byte[] BufferFromImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            Byte[] buffer = null;
            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }

        public Database()
        {
            this.conn = new SqlConnection(@"Data Source=localhost;Initial Catalog = IPD20_Library; User ID = sa; Password=11");
        }

        public void UPdateData(Person person)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("UPDATE Members.Member SET Photograph=@img_data, FirstName=@name, Age=@age WHERE MemberNo=@memberNo", conn);
            //////////             

            SqlParameter param0 = new SqlParameter("@img_data", SqlDbType.Image);
            if(person.ImageData==null)
            {
                param0.Value = DBNull.Value;
            }
            else
                param0.Value = person.ImageData;
            command.Parameters.Add(param0);

            SqlParameter param1 = new SqlParameter("@name", SqlDbType.NVarChar);
            param1.Value = person.Name;
            command.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter("@age", SqlDbType.Int);
            param2.Value = person.Age;
            command.Parameters.Add(param2);

            SqlParameter param3 = new SqlParameter("@memberNo", SqlDbType.Int);
            param3.Value = person.MemberNo;
            command.Parameters.Add(param3);

            int numRowsAffected = command.ExecuteNonQuery();
            
            //////////
            command.Dispose();
            conn.Close();
        }

        public ObservableCollection<Person> ReadData()
        {
            ObservableCollection<Person> list = new ObservableCollection<Person>();
            conn.Open();
            SqlCommand comd = new SqlCommand("select * from Members.Member", conn);
            SqlDataReader read = comd.ExecuteReader();
            while (read.Read())
            {
                if (!int.TryParse(read.GetValue(0).ToString(), out int no))
                {
                    no = 0;
                }
                string name = read.GetValue(2).ToString();
                //int age = int.Parse(read.GetValue(4).ToString());
                if (!int.TryParse(read.GetValue(4).ToString(), out int age))
                {
                    age = 0;
                }
                //var image = new BitmapImage();
                Byte[] photo = null;
                if (!DBNull.Value.Equals(read.GetValue(3)))
                {
                    photo = (Byte[])read.GetValue(3);
                }

                Person p = new Person(no, name, age, photo);
                list.Add(p);
            }
            read.Close();
            comd.Dispose();
            conn.Close();
            return list;
        }
    }
}
