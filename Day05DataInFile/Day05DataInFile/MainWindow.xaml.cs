﻿using Microsoft.Win32;
using System.IO;
using System.Windows;

namespace Day05DataInFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string filename = "";

        private void miFileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
                    textChenged = false;
                    filename = openFileDialog.FileName;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "File reading error \n" + ex.Message, "File error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
                
        }

        private void miFileSaveAs_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    File.WriteAllText(saveFileDialog.FileName, txtEditor.Text);
                    textChenged = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "File writing error \n" + ex.Message, "File error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
                
        }

        bool textChenged = false;

        private void txtEditor_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            textChenged = true;
        }

        private void miFileSave_MenuClick(object sender, RoutedEventArgs e)
        {
            if (filename != "")
            {
                try
                {
                    File.WriteAllText(filename, txtEditor.Text);
                    textChenged = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "File writing error \n" + ex.Message, "File error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        File.WriteAllText(saveFileDialog.FileName, txtEditor.Text);
                        textChenged = false;
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show(this, "File writing error \n" + ex.Message, "File error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void miFileNew_MenuClick(object sender, RoutedEventArgs e)
        {
            if (textChenged)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to create a new file without saving?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    txtEditor.Text = "";
                    filename = "";
                    textChenged = false;
                }
            }
        }

        private void miFileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            if (textChenged)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to exit without saving?", "Exit Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    System.Windows.Application.Current.Shutdown();
                }
                else
                {
                    miFileSaveAs_MenuClick(sender, new RoutedEventArgs());
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (textChenged)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to exit without saving?", "Exit Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    miFileSaveAs_MenuClick(sender, new RoutedEventArgs());
                    e.Cancel = true;
                }
                    
            }
        }
    }
}
