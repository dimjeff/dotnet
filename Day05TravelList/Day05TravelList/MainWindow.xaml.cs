﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05TravelList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Trip> tripList = new List<Trip>();
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;
        const string filename = @"..\..\data.txt";

        public MainWindow()
        {
            InitializeComponent();
            lvDataBinding.ItemsSource = tripList;
            loadDataFromFile();
        }

        private void tfPassport_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
/*            TextBox tb = (TextBox)sender;
            Regex regex = new Regex(@"[A-Z]{2}\d{6}");
            e.Handled = !regex.IsMatch(tb.Text + e.Text);*/
        }

        Trip selectedTrip;

        private void lvDataBinding_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                btAdd.IsEnabled = false;
                btUpdate.IsEnabled = true;
                btDelete.IsEnabled = true;
                selectedTrip = (Trip)lvDataBinding.SelectedItem;//but listview is multiple selection
                tfName.Text = selectedTrip.Name;
                tfDestination.Text = selectedTrip.Destination;
                tfPassport.Text = selectedTrip.Passport;
                dtpDeparture.Text = selectedTrip.DepartureDate.ToString();
                dtpReturn.Text = selectedTrip.ReturnDate.ToString();
            }
            else
            {
                selectedTrip = null;
                tfName.Text = "";
                tfDestination.Text = "";
                tfPassport.Text = "";
                dtpDeparture.Text = "";
                dtpReturn.Text = "";
                btAdd.IsEnabled = true;
                btUpdate.IsEnabled = false;
                btDelete.IsEnabled = false;
            }
        }

        private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvDataBinding.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvDataBinding.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string destination = tfDestination.Text;
                string name = tfName.Text;
                string passport = tfPassport.Text;
                DateTime depDate = DateTime.Parse(dtpDeparture.Text);
                DateTime retDate = DateTime.Parse(dtpReturn.Text);
                Trip tp = new Trip(destination, name, passport, depDate, retDate);
                tripList.Add(tp);
                lvDataBinding.Items.Refresh();
            }catch(FormatException ex)
            {
                MessageBox.Show(this, "Adding data error:\n" + ex.Message, "Error to add", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(this, "Adding data error:\n" + ex.Message, "Error to add", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTrip == null)
            {
                return;
            }
            try
            {
                string destination = tfDestination.Text;
                string name = tfName.Text;
                string passport = tfPassport.Text;
                DateTime depDate = DateTime.Parse(dtpDeparture.Text);
                DateTime retDate = DateTime.Parse(dtpReturn.Text);
                selectedTrip.Destination = destination;
                selectedTrip.Name = name;
                selectedTrip.Passport = passport;
                selectedTrip.setDepartureAndReturnDates(depDate, retDate);
                lvDataBinding.Items.Refresh();
            }
            catch (FormatException ex)
            {
                MessageBox.Show(this, "Updating data error:\n" + ex.Message, "Error to update", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this, "Updating data error:\n" + ex.Message, "Error to update", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTrip == null)
            {
                return;
            }
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + selectedTrip, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.OK)
            {
                tripList.Remove(selectedTrip);
                lvDataBinding.Items.Refresh();
            }
        }

        private void loadDataFromFile()
        {
            try
            {
                string[] trips = File.ReadAllLines(filename);
                string errmsg = "";
                foreach (string line in trips)
                {
                    string[] linedata = line.Split(';');
                    if (linedata.Length != 5)
                    {
                        errmsg += line + "\n";
                        continue;
                    }
                    try
                    {
                        DateTime depDate = DateTime.Parse(linedata[3]);
                        DateTime retDate = DateTime.Parse(linedata[4]);

                        Trip tp = new Trip(linedata[0], linedata[1], linedata[2], depDate, retDate);
                        tripList.Add(tp);

                    }
                    catch (FormatException ex)
                    {
                        errmsg += line + "\n";
                    }
                    catch(ArgumentException ex)
                    {
                        errmsg += line + "\n";
                    }

                }
                if (errmsg != "")
                {
                    MessageBox.Show(this, "Import data errors:\n" + errmsg, "Error import data", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (IOException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
        }

        private void saveDataToFile()
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                string[] dataLine = new string[lvDataBinding.SelectedItems.Count];
                int index = 0;
                foreach(Trip tp in lvDataBinding.SelectedItems)
                {
                    dataLine[index++] = tp.ToDataString();
                }
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        File.WriteAllLines(saveFileDialog.FileName, dataLine);
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show(this, "Error writing file:\n" + ex.Message, "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
            }
            else
            {
                MessageBox.Show(this, "Please select data first", "Data select", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            saveDataToFile();
        }
    }
}
