﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carsList = new List<Car>();
        public MainWindow()
        {
            InitializeComponent();
            lvDataBinding.ItemsSource = carsList;
            displayStatus();
        }        

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(null);
            if (dialog.ShowDialog() == true)
            {
                if(dialog.Car != null)
                {
                    if(!dialog.IsEdit)
                        carsList.Add(dialog.Car);
                    lvDataBinding.Items.Refresh();
                    displayStatus();
                }
            }
        }

        private void lvDataBinding_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog((Car)lvDataBinding.SelectedItem);
            if (dialog.ShowDialog() == true)
            {
                if (dialog.Car != null)
                {
                    if (!dialog.IsEdit)
                        carsList.Add(dialog.Car);
                    lvDataBinding.Items.Refresh();
                    displayStatus();
                }
            }
        }

        private void displayStatus()
        {
            lblCursorPosition.Text = "You have "+ carsList.Count + " cars";
        }

        private void miEdit_Context(object sender, RoutedEventArgs e)
        {
            if(lvDataBinding.SelectedIndex != -1)
            {
                lvDataBinding_MouseDoubleClick(sender, new MouseButtonEventArgs(InputManager.Current.PrimaryMouseDevice, 2, MouseButton.Right));
            }
        }

        private void miDelete_Context(object sender, RoutedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + lvDataBinding.SelectedItem, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    carsList.Remove((Car)lvDataBinding.SelectedItem);
                    lvDataBinding.Items.Refresh();
                    displayStatus();
                }
            }
        }

        private void miFileExportTo_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
/*                string[] arr = new string[carsList.Count];
                for(int i=0; i< carsList.Count; i++)
                {
                    arr[i] = ((Car)carsList[i]).ToString();
                }*/
                using (var writer = new StreamWriter(saveFileDialog.FileName))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(carsList);
                }
                /*                try
                                {
                                    File.WriteAllLines(saveFileDialog.FileName, arr);
                                }
                                catch (IOException ex)
                                {
                                    MessageBox.Show(this, "File writing error \n" + ex.Message, "File error", MessageBoxButton.OK, MessageBoxImage.Error);
                                }*/
            }
        }

        private void miFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
