﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    /// 
    enum BreadType
    {
        AAA,BBB
    }

    public partial class CustomDialog : Window
    {
        string bread, meat, veggies;

        public string Bread{
            get
            {
                return bread;
            }
        }

        public string Veggies
        {
            get
            {
                return veggies;
            }
        }

        public string Meat
        {
            get
            {
                return meat;
            }
        }

        public CustomDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            bread = cmBread.Text;
            ChildControls ccChildren = new ChildControls();
            string regg = "";
            List<string> vegg = new List<string>();
            foreach (object o in ccChildren.GetChildren(InputGroup, 1))
            {
                if (o.GetType() == typeof(CheckBox))
                {
                    CheckBox checkbox = (CheckBox)o;
                    if (checkbox.IsChecked == true)
                        vegg.Add(checkbox.Content.ToString());
                }
            }
            veggies = string.Join(",", vegg.ToArray());
            foreach (object o in ccChildren.GetChildren(OutputGroup, 1))
            {
                if (o.GetType() == typeof(RadioButton))
                {
                    RadioButton radio = (RadioButton)o;
                    if (radio.IsChecked == true)
                    {
                        meat = radio.Content.ToString();
                        break;
                    }
                }
            }

            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }

    class ChildControls
    {
        private List<object> lstChildren;

        public List<object> GetChildren(Visual p_vParent, int p_nLevel)
        {
            if (p_vParent == null)
            {
                throw new ArgumentNullException("Element {0} is null!", p_vParent.ToString());
            }

            this.lstChildren = new List<object>();

            this.GetChildControls(p_vParent, p_nLevel);

            return this.lstChildren;

        }

        private void GetChildControls(Visual p_vParent, int p_nLevel)
        {
            int nChildCount = VisualTreeHelper.GetChildrenCount(p_vParent);

            for (int i = 0; i <= nChildCount - 1; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(p_vParent, i);

                lstChildren.Add((object)v);

                if (VisualTreeHelper.GetChildrenCount(v) > 0)
                {
                    GetChildControls(v, p_nLevel + 1);
                }
            }
        }
    }
}
