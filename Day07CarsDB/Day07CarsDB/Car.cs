﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day06CarsWithDialog
{
    public class Car
    {
        int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        string _makemodel;
        public string MakeModel {
            set 
            {
                Regex rgx = new Regex("^[^;]{2,15}$");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The MakeModel must be 2-15 letters, and no semicolons.");
                }
                _makemodel = value;
            }

            get 
            {
                return _makemodel;
            } 
        } // 2-50 characters, no semicolons

        double _enginesizel;
        public double EngineSizeL 
        {
            get
            {
                return _enginesizel;
            }
            set
            {
                if (value>20 || value <0)
                {
                    throw new ArgumentException("The EngineSize must between 0-20.");
                }
                _enginesizel = value;
            }
        } // 0-20

        FuelTypeEnum _fueltype;
        public FuelTypeEnum FuelType 
        {
            get
            {
                return _fueltype;
            }
            set
            {
                _fueltype = value;
            }
        }

        byte[] _photo;
        public byte[] Photo
        {
            get { return this._photo; }
            set { this._photo = value; }
        }
        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", MakeModel, EngineSizeL, FuelType);
        }
    }
}
