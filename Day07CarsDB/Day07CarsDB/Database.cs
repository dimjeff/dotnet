﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Day06CarsWithDialog
{
    public class Database
    {
        protected readonly SqlConnection conn;

        public static ImageSource ByteToImage(byte[] imageData)
        {
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch (NotSupportedException ex)
            {
                return null;
            }
        }

        public static Byte[] BufferFromImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            Byte[] buffer = null;
            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }

        public Database()
        {
            this.conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\trakadmin\Documents\dotnet\Day07CarsDB\CarsDB.mdf;Integrated Security=True;Connect Timeout=30");
        }

        public void DeleteCar(Car car)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("DELETE FROM Cars WHERE Id=@id", conn);
            //////////             

            command.Parameters.AddWithValue("@id", car.Id);
            command.ExecuteNonQuery();

            //////////
            command.Dispose();
            conn.Close();
        }

        public int AddNewCar(Car car)
        {
            int addID = 0;
            conn.Open();
            SqlCommand command = new SqlCommand("INSERT INTO Cars(Photo,MakeModel,EngineSize,FuleType) VALUES(@img,@makeModel,@engineSize,@fuleType);SELECT SCOPE_IDENTITY();", conn);
            //////////             

            SqlParameter param0 = new SqlParameter("@img", SqlDbType.Image);
            if (car.Photo == null)
            {
                param0.Value = DBNull.Value;
            }
            else
                param0.Value = car.Photo;
            command.Parameters.Add(param0);
            //command.Parameters.AddWithValue("@img", param0);
            command.Parameters.AddWithValue("@makeModel", car.MakeModel);
            command.Parameters.AddWithValue("@engineSize", car.EngineSizeL);
            command.Parameters.AddWithValue("@fuleType", car.FuelType.ToString());

            object returnObj = command.ExecuteScalar();

            if (returnObj != null)
            {
                int.TryParse(returnObj.ToString(), out addID);
            }


            //////////
            command.Dispose();
            conn.Close();
            return addID;
        }

        public void UPdateData(Car car)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("UPDATE Cars SET Photo=@img, MakeModel=@makeModel, EngineSize=@engineSize,FuleType=@fuleType WHERE Id=@id", conn);
            //////////             

            SqlParameter param0 = new SqlParameter("@img", SqlDbType.Image);
            if (car.Photo == null)
            {
                param0.Value = DBNull.Value;
            }
            else
                param0.Value = car.Photo;
            command.Parameters.Add(param0);
            //command.Parameters.AddWithValue("@img", param0);
            command.Parameters.AddWithValue("@makeModel",car.MakeModel);
            command.Parameters.AddWithValue("@engineSize", car.EngineSizeL);
            command.Parameters.AddWithValue("@fuleType", car.FuelType.ToString());
            command.Parameters.AddWithValue("@id", car.Id);
            command.ExecuteNonQuery();

            //////////
            command.Dispose();
            conn.Close();
        }

        public List<Car> ReadData()
        {
            List<Car> list = new List<Car>();
            conn.Open();
            SqlCommand comd = new SqlCommand("select * from Cars", conn);
            SqlDataReader read = comd.ExecuteReader();
            while (read.Read())
            {
                int id = read.GetInt32(read.GetOrdinal("Id"));
                string makeModel = read.GetString(1);
                double engineSize = read.GetDouble(2);

                if (!Enum.TryParse(read.GetString(3), out Car.FuelTypeEnum fuelType))
                {
                    throw System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(SqlException)) as SqlException;
                }
                Byte[] photo = null;
                if (!DBNull.Value.Equals(read[4]))
                {
                    photo = (byte[])read[4];
                }

                Car car = new Car(){ Id = id, MakeModel = makeModel, EngineSizeL = engineSize, FuelType = fuelType, Photo = photo};
                list.Add(car);
            }
            read.Close();
            comd.Dispose();
            conn.Close();
            return list;
        }
    }
}
