﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public delegate double calculateDelegate(double d1,double d2);
    public partial class MainWindow : Window
    {
        int status = 0;
        const string filename = @"..\..\data.txt";
        public static calculateDelegate calculator;
        public MainWindow()
        {
            InitializeComponent();
        }


        private TextBox handleDisplayTextBox()
        {
            switch (status%3)
            {
                case 0:
                    tbFirstValue.IsEnabled = true;
                    tbSecendValue.IsEnabled = false;
                    tbResult.IsEnabled = false;
                    return tbFirstValue;
                case 1:
                    tbFirstValue.IsEnabled = false;
                    tbSecendValue.IsEnabled = true;
                    tbResult.IsEnabled = false;
                    return tbSecendValue;
                case 2:
                    tbFirstValue.IsEnabled = false;
                    tbSecendValue.IsEnabled = false;
                    tbResult.IsEnabled = false;
                    return null;
                default:
                    return null;
            }
            return null;
        }

        private void btNumber_Click(object sender, RoutedEventArgs e)
        {
            TextBox tb = handleDisplayTextBox();
            if (tb == null)
                return;
            string tbText = tb.Text;
            string btText = ((Button)sender).Content.ToString();
            switch (btText)
            {
                case ".":
                    tb.Text += tbText.IndexOf('.') != -1 ? "" : ".";
                    break;
                case "+/-":
                    tb.Text = double.Parse(tbText==""?"0": tbText) * -1 + "";
                    break;
                default:
                    tb.Text += btText;
                    break;

            }
        }

        private void btOperation_Click(object sender, RoutedEventArgs e)
        {
            lblOperation.Content = ((Button)sender).Content;
            status=1;
            handleDisplayTextBox();
        }

        private void btResult_Click(object sender, RoutedEventArgs e)
        {
            switch (lblOperation.Content.ToString())
            {
                case "+":
                    calculator = addNumbers;                   
                    break;
                case "-":
                    calculator = MinusNumbers;
                    break;
                case "*":
                    calculator = MultiplyNumbers;
                    break;
                case "/":
                    if (double.Parse(tbSecendValue.Text) == 0.0)
                        return;
                    calculator = DivideNumbers;
                    break;
            }
            tbResult.Text = calculator(double.Parse(tbFirstValue.Text), double.Parse(tbSecendValue.Text)) + "";
            status =2;
            handleDisplayTextBox();
            try
            {
                File.AppendAllText(filename, tbFirstValue.Text + lblOperation.Content.ToString()+ tbSecendValue.Text+" = " + tbResult.Text + Environment.NewLine);
            }
            catch (IOException ex)
            {
                MessageBox.Show(this, "Error writing file:\n" + ex.Message, "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            tbFirstValue.Text = "";
            tbSecendValue.Text = "";
            tbResult.Text = "";
            lblOperation.Content = "?";
            status = 0;
            handleDisplayTextBox();
        }

        private double addNumbers(double d1,double d2)
        {
            return d1 + d2;
        }

        private double MinusNumbers(double d1, double d2)
        {
            return d1 - d2;
        }

        private double MultiplyNumbers(double d1, double d2)
        {
            return d1 * d2;
        }

        private double DivideNumbers(double d1, double d2)
        {
            return d1 / d2;
        }
    }
}
