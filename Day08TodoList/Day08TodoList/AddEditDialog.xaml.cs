﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Todo _todo;

        public Todo Todo
        {
            get
            {
                return _todo;
            }
        }
        public bool IsEdit = false;
        public AddEditDialog(Todo todo)
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;

            if (todo == null)
            {
                btAddSave.Content = "Add";
            }
            else
            {
                tfTask.Text = todo.Task;
                dpDuedate.Text = todo.Duedate.ToString("d");
                ckIsdone.IsChecked = todo.StatusType.ToString() == "Done" ? true : false;
                tbID.Text = todo.Id + "";
                btAddSave.Content = "Edit";
            }

            _todo = todo;
        }

        private void btAddSave_Click(object sender, RoutedEventArgs e)
        {
            string task = tfTask.Text;
            if(!DateTime.TryParse(dpDuedate.Text,out DateTime duedate))
            {
                MessageBox.Show(this, "The due date is invalid\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string isdone = ckIsdone.IsChecked == true ? "Done" : "Pending";
            if (!Enum.TryParse(isdone, out Todo.StatusEnum status))
            {
                MessageBox.Show(this, "The status type is invalid.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_todo == null)
            {
                IsEdit = false;
                try
                {

                    Todo todo = new Todo() { Id = 0, Task = task, Duedate = duedate, StatusType = status};
                    _todo = todo;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, "Todo can not be created:\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                try
                {
                    IsEdit = true;

                    _todo.Task = task;
                    _todo.Duedate = duedate;
                    _todo.StatusType = status;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, "Todo can not be created:\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            DialogResult = true;
        }

    }

}
