﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Day06CarsWithDialog
{
    class DatabaseException : Exception
    {
        public DatabaseException(string msg, Exception innerException) : base(msg, innerException) { }
    }

    class Database
    {
        SqlConnection conn;


        public Database()
        {
            try
            {
                this.conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\trakadmin\Documents\dotnet\Day08TodoList\Todos.mdf;Integrated Security=True;Connect Timeout=30");
                conn.Open();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Database connecting is error: " + ex.Message, ex);
            }
        }

        public void CloseConnecting()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        public void Delete(Todo todo)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM Todos WHERE Id=@id", conn);
                //////////             

                command.Parameters.AddWithValue("@id", todo.Id);
                command.ExecuteNonQuery();

                //////////
                command.Dispose();
            }catch(SqlException ex)
            {
                throw new DatabaseException("Deleting data error: "+ex.Message, ex);
            }
        }

        public int AddNew(Todo todo)
        {
            int addID = 0;
            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO Todos(Task,DueDate,Status) VALUES(@task,@duedate,@status);SELECT SCOPE_IDENTITY();", conn);
                //////////             
                command.Parameters.AddWithValue("@task", todo.Task);
                command.Parameters.AddWithValue("@duedate", todo.Duedate);
                command.Parameters.AddWithValue("@status", todo.StatusType.ToString());

                object returnObj = command.ExecuteScalar();

                if (returnObj != null)
                {
                    int.TryParse(returnObj.ToString(), out addID);
                }

                //////////
                command.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Adding data error: " + ex.Message, ex);
            }
            return addID;
        }

        public void UPdateData(Todo todo)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Todos SET Task=@task, DueDate=@duedate,Status=@status WHERE Id=@id", conn);
                //////////             
                command.Parameters.AddWithValue("@task", todo.Task);
                command.Parameters.AddWithValue("@duedate", todo.Duedate);
                command.Parameters.AddWithValue("@status", todo.StatusType.ToString());
                command.Parameters.AddWithValue("@id", todo.Id);
                command.ExecuteNonQuery();

                //////////
                command.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Updating data error: " + ex.Message, ex);
            }
        }

        public List<Todo> ReadAllData()
        {
            List<Todo> list = new List<Todo>();
            try
            {
                SqlCommand comd = new SqlCommand("select * from Todos", conn);
                SqlDataReader read = comd.ExecuteReader();
                while (read.Read())
                {
                    int id = read.GetInt32(read.GetOrdinal("Id"));
                    string task = read.GetString(read.GetOrdinal("Task"));
                    DateTime duedate = read.GetDateTime(read.GetOrdinal("DueDate"));

                    if (!Enum.TryParse(read.GetString(read.GetOrdinal("Status")), out Todo.StatusEnum status))
                    {
                        throw System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(SqlException)) as SqlException;
                    }

                    Todo td = new Todo(){ Id = id, Task = task, Duedate = duedate, StatusType = status};
                    list.Add(td);
                }
                read.Close();
                comd.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Reading data error: " + ex.Message, ex);
            }
            return list;
        }
    }
}
