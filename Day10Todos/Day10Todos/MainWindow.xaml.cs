﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10Todos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SocietyDBContextAgain ctx = new SocietyDBContextAgain();

        public MainWindow()
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatsEnum));

            getAllData();
        }

        private void getAllData()
        {
            lvDataBinding.ItemsSource = (from p in ctx.Todos select p).ToList();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string task = tbTask.Text;
            int diff = (int)sliderDiff.Value;

            if (!Enum.TryParse(comboStatus.Text, out Todo.StatsEnum status))
            {
                //MessageBox.Show(this, "The status must be selected.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                //return;
            }
            if (!DateTime.TryParse(dpDuedate.Text, out DateTime duedate))
            {
                //Todo td = new Todo() { Id = 0, Task = task, Difficulty = diff,  Status = status };
                //ctx.Todos.Add(td);
                //ctx.SaveChanges();
            }
            else
            {
                //Todo td = new Todo() { Id = 0, Task = task, Difficulty = diff, Status = status, DueDate=duedate };
                //ctx.Todos.Add(td);
                //ctx.SaveChanges();
            }
            try
            {
                Todo td = new Todo() { Id = 0, Task = task, Difficulty = diff, Status = status, DueDate = duedate };
                ctx.Todos.Add(td);
                ctx.SaveChanges();
            }
            catch(SystemException ex)
            {                
                MessageBox.Show(this, "Adding cannot finished: \n"+ ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            getAllData();
        }

        Todo selected = null;

        private void btDel_Click(object sender, RoutedEventArgs e)
        {
            if (selected != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + selected, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    ctx.Todos.Remove(selected);
                    ctx.SaveChanges();
                }
            }
            getAllData();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (selected != null)
            {
                int id = int.Parse(tbId.Text);
                string task = tbTask.Text;
                selected.Task = task;
                int diff = (int)sliderDiff.Value;
                selected.Difficulty = diff;
                if (Enum.TryParse(comboStatus.Text, out Todo.StatsEnum status))
                {
                    selected.Status = status;
                    //MessageBox.Show(this, "The status must be selected.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //return;
                }
                
                if (DateTime.TryParse(dpDuedate.Text, out DateTime duedate))
                {
                    selected.DueDate = duedate;
                }
                ctx.SaveChanges();
            }
            getAllData();
        }

        private void lvDataBinding_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selected = (Todo)lvDataBinding.SelectedItem;
            if (selected != null)
            {
                tbId.Text = selected.Id + "";
                tbTask.Text = selected.Task;
                sliderDiff.Value = selected.Difficulty;
                dpDuedate.Text = selected.DueDate.ToString();
                comboStatus.Text = selected.Status.ToString();
                btAdd.IsEnabled = false;
                btDel.IsEnabled = true;
                btUpdate.IsEnabled = true;
            }
            else
            {
                tbId.Text = "";
                tbTask.Text = "";
                sliderDiff.Value = 1;
                dpDuedate.Text = "";
                comboStatus.Text = "";
                btAdd.IsEnabled = true;
                btDel.IsEnabled = false;
                btUpdate.IsEnabled = false;
            }
        }
    }

    [Table("Todos")]
    class Todo
    {
        public enum StatsEnum { Done=1, Pending=0}

        [Key]
        public int Id { get; set; }

        [Required] // means not-null
        [StringLength(50)] // nvarchar(50)
        [Index]
        public string Task { get; set; }

        public int Difficulty { get; set; }

        public DateTime DueDate { get; set; }

        [EnumDataType(typeof(StatsEnum))]
        public StatsEnum Status { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}",Id,Task);
        }
    }

    class SocietyDBContextAgain : DbContext
    {
        virtual public DbSet<Todo> Todos { get; set; }
    }


}
