﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsManagerDialog.xaml
    /// </summary>
    public partial class CarsManagerDialog : Window
    {
        Owner currentOwner;
        CarsOwnerContext ctx = new CarsOwnerContext();
        public CarsManagerDialog(Owner owner)
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            if(owner == null)
            {
                DialogResult = true;
                return;
            }
                
            currentOwner = owner;
            tbOwnerName.Text = currentOwner.Name;
            getAllData();
        }

        private void getAllData()
        {
            int ownerid = currentOwner.Id;
            lvDataBinding.ItemsSource = (from o in ctx.Cars where o.OwnerId == ownerid select o).ToList();
        }
        public Car selected = null;
        private void lvDataBinding_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                selected = (Car)lvDataBinding.SelectedItem;
                tbId.Text = selected.Id + "";
                tbMakeModel.Text = selected.MakeModel;
            }
            else
            {
                selected = null;
                tbId.Text = "";
                tbMakeModel.Text = "";
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string makemodel = tbMakeModel.Text;

            try
            {
                Car car = new Car() { MakeModel = makemodel, OwnerId = currentOwner.Id };
                ctx.Cars.Add(car);
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            getAllData();
        }

        private void btDel_Click(object sender, RoutedEventArgs e)
        {
            if (selected != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + selected, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    try
                    {
                        ctx.Cars.Remove(selected);
                        ctx.SaveChanges();
                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(this, "Deletion cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            getAllData();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (selected != null)
            {
                try
                {
                    selected.MakeModel = tbMakeModel.Text;
                    ctx.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "Updating cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            getAllData();
        }

        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
