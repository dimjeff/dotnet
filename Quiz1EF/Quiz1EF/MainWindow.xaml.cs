﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1EF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static public PersonPassportContext ctx = new PersonPassportContext();
        public MainWindow()
        {
            InitializeComponent();
            /*          ctx.Database.Delete();
            ctx.Database.Create();*/
            getAllData();
        }

        private void getAllData()
        {
            lvDataBinding.ItemsSource = (from p in ctx.People select p).ToList();
        }

        private void lvDataBinding_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                Person selected = (Person)lvDataBinding.SelectedItem;
                if (selected == null)
                    return;
                PassportDialog ppd = new PassportDialog(selected);
                if (ppd.ShowDialog() == true)
                {
                    getAllData();
                }
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            PersonDialog pd = new PersonDialog();
            if (pd.ShowDialog() == true)
            {
                getAllData();
            }
        }
    }
}
