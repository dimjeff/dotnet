﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1EF
{
    [Table("Passports")]
    public class Passport
    {
        [ForeignKey("Person")]
        public int Id { set; get; }

        [Required]
        [StringLength(8)]
        public string PassportNo { set; get; }

        public byte[] Photo { get; set; }

        public virtual Person Person { get; set; }
    }
}
