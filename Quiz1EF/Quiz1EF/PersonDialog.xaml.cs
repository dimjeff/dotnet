﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz1EF
{
    /// <summary>
    /// Interaction logic for PersonDialog.xaml
    /// </summary>
    public partial class PersonDialog : Window
    {
        public PersonDialog()
        {
            InitializeComponent();
        }

        private void tbAge_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if (name.Length==0 || name.Length > 150)
            {
                MessageBox.Show(this, "The name must be 1-150 chars.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(!int.TryParse(tbAge.Text,out int age)){
                MessageBox.Show(this, "The age must be a number.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                Person person = new Person() { Name = name, Age = age };
                MainWindow.ctx.People.Add(person);
                MainWindow.ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DialogResult = true;
        }
    }
}
