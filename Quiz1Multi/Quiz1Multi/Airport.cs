﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Multi
{
	public class InvalidDataException : Exception
	{
		public InvalidDataException()
		{
		}

		public InvalidDataException(string message)
			: base(message)
		{
		}
	}

	public delegate void LoggerDelegate(string msg);

	class Airport
    {
		public static LoggerDelegate Logger;

		public Airport(string code, string city, double lat, double lng, int elevM) {
			Code = code;
			City = city;
			Latitude = lat;
			Longitude = lng;
			ElevationMeters = elevM;
		}

		public Airport(string dataLine) {
			string[] strArr = dataLine.Split(';');

			if (strArr.Length != 5)
			{
				if (Logger != null)
				{
					Logger("Create Airport failed, reading invalid data from file: " + dataLine);
				}
				throw new InvalidDataException("The data line invalid: " + dataLine);
			}
			try
			{
				Code = strArr[0];
				City = strArr[1];
			}
			catch
			{
				throw new InvalidDataException("The data line invalid: " + dataLine);
			}


			if (!double.TryParse(strArr[2], out double latitude))
			{
				if (Logger != null)
				{
					Logger("Create Airport failed, reading invalid latitude from file: " + dataLine);
				}
				throw new InvalidDataException("The latitude invalid: " + dataLine);
			}
			Latitude = latitude;

			if (!double.TryParse(strArr[3], out double longitude))
			{
				if (Logger != null)
				{
					Logger("Create Airport failed, reading invalid longitude from file: " + dataLine);
				}
				throw new InvalidDataException("The longitude invalid: " + dataLine);
			}
			Longitude = longitude;

			if (!int.TryParse(strArr[4], out int elevation))
			{
				if (Logger != null)
				{
					Logger("Create Airport failed, reading invalid elevation from file: " + dataLine);
				}
				throw new InvalidDataException("The elevation invalid: " + dataLine);
			}
			ElevationMeters = elevation;
		}

		string _code;
		public string Code
		{// exactly 3 uppercase letters, use regexp
			get
			{
				return _code;
			}
			set
			{
				Regex rgx = new Regex("^[A-Z]{3}$");
				if (!rgx.IsMatch(value))
				{
					if (Logger != null)
					{
						Logger(string.Format("Create Airport failed, code invalid: {0}",value)); 
					}
					throw new InvalidDataException("The code must be exactly 3 uppercase letters. ");
				}
				_code = value;
			}
		}

		string _city;
		public string City
		{
			// 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
			get
			{
				return _city;
			}
			set
			{
				Regex rgx = new Regex(@"^[A-Za-z\d.,-]{1,50}$");
				if (!rgx.IsMatch(value))
				{
					if (Logger != null)
					{
						Logger(string.Format("Create Airport failed, city invalid: {0}", value));
					}
					throw new InvalidDataException("The city must be 1-50 characters, digits, and .,- characters:");
				}
				_city = value;
			}
		}

		double _latitude, _longitude;
		public double Latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				if (value>90.0 || value<-90.0)
				{
					if (Logger != null)
					{
						Logger(string.Format("Create Airport failed, latitude invalid: {0}", value));
					}
					throw new InvalidDataException("The latitude must be from -90 to 90.");
				}
				_latitude = value;
			}
		}

		public double Longitude
		{// -90 to 90, -180 to 180
			get
			{
				return _longitude;
			}
			set
			{
				if (value > 180.0 || value < -180.0)
				{
					if (Logger != null)
					{
						Logger(string.Format("Create Airport failed, longitude invalid: {0}", value));
					}
					throw new InvalidDataException("The longitude must be from -180 to 180.");
				}
				_longitude = value;
			}
		}
		int _elevationmeters;
		public int ElevationMeters
		{
			//-1000 to 10000
			get
			{
				return _elevationmeters;
			}
			set
			{
				if (value > 10000.0 || value < -1000.0)
				{
					if (Logger != null)
					{
						Logger(string.Format("Create Airport failed, elevation invalid: {0}", value));
					}
					throw new InvalidDataException("The elevation must be from -1000 to 10000.");
				}
				_elevationmeters = value;
			}
		}


		public override string ToString() {
			return string.Format("{0} in {1} at {2} lat / {3} lng at {4}m elevation", Code, City, Latitude, Longitude, ElevationMeters);
		}

		public string ToDataString() {
			return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
		}
	}
}
