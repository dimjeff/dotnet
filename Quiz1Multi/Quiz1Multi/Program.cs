﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Multi
{
    class Program
    {
        const string filename = @"..\..\data.txt";
        static List<Airport> AirportsList = new List<Airport>();

        static void Main(string[] args)
        {
            ReadDataFromFile();

            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddAirport();
                        break;
                    case 2:
                        ListAirport();
                        break;
                    case 3:
                        NearestDistance();
                        break;
                    case 4:
                        GetDeviation();
                        break;
                    case 5:
                        ChangelogDelegate();
                        break;
                    case 0:
                        Console.WriteLine("Good bye!");
                        WriteDataToFile();
                        System.Environment.Exit(-1);
                        break;
                    default:
                        Console.WriteLine("Wrong choice, try again.");
                        break;
                }
            }
        }

        static void GetDeviation()
        {
            Console.WriteLine("");
            double average = (from ap in AirportsList select ap.ElevationMeters).Average();
            double sumOfSquaresOfDifferences = (from ap in AirportsList select ap.ElevationMeters).Select(val => (val - average) * (val - average)).Sum();
            double sd = Math.Sqrt(sumOfSquaresOfDifferences / (from ap in AirportsList select ap.ElevationMeters).Count());
            Console.WriteLine(string.Format("For all airports the standard deviation of their elevation is {0:N2}", sd));
            Console.WriteLine("");
        }

        static void ChangelogDelegate()
        {
            Console.WriteLine("");
            while (true)
            {
                string menu;
                Console.Write("1-Logging to console\n" +
                            "2-Logging to file\n" +
                            "Enter your choices, comma-separated, empty for none:");
                menu = Console.ReadLine();
                Regex rgx = new Regex("^[12,]{1,3}$");
                if (!rgx.IsMatch(menu))
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                string[] arr = menu.Split(',');
                if (arr.Length == 0)
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                string text = "";
                //bool existing = false;
                foreach(string str in arr)
                {
                    if (str == "1")
                    {
                        Airport.Logger += LogToConsole;
                        text += "Logging to console enabled\n";
                    }
                    if (str == "2")
                    {
                        Airport.Logger += LogToFile;
                        text += "Logging to file enabled\n";
                    }
                }

                Console.WriteLine(text);
                break;
            }
            Console.WriteLine("");
        }

        static void NearestDistance()
        {
            if (AirportsList.Count < 2)
            {
                Console.WriteLine("No more then 2 airport. ");
                return;
            }

            double f1, f2, s1, s2;
            Airport ap1 = null, ap2 = null;
            string airport1;
            while (true)
            {
                Console.Write("Please enter an ariport code:");
                airport1 = Console.ReadLine();
                foreach (Airport ap in AirportsList)
                {
                    if (ap.Code == airport1)
                    {
                        ap1 = ap;
                        break;
                    }
                }
                if (ap1 == null)
                {
                    Console.WriteLine("Invalid code, can not find it, please try again. ");
                    continue;
                }
                f1 = ap1.Longitude;
                f2 = ap1.Latitude;
                break;
            }
            double distance = 0.0;
            foreach (Airport ap in AirportsList)
            {
                if (ap.Code == airport1)
                    continue;
                s1 = ap.Longitude;
                s2 = ap.Latitude;
                double temp = Distance(f1, f2, s1, s2);
                if (distance == 0.0 || distance > temp)
                {
                    ap2 = ap;
                    distance = temp;
                }
            }
            Console.WriteLine("");
            Console.WriteLine(string.Format("The nearest airport to {0} is {1}, distance is {2:N2}km\n", airport1, ap2.Code, distance));
        }

        static double Distance(double long1, double lat1, double long2, double lat2)
        {
            /*            var sCoord = new GeoCoordinate(lat1, long1);
                        var eCoord = new GeoCoordinate(lat2, long2);

                        return sCoord.GetDistanceTo(eCoord);*/
            double a, b, R;
            R = 6371.393;
            lat1 = lat1 * Math.PI / 180.0;
            lat2 = lat2 * Math.PI / 180.0;
            a = lat1 - lat2;
            b = (long1 - long2) * Math.PI / 180.0;
            double d;
            double sa2, sb2;
            sa2 = Math.Sin(a / 2.0);
            sb2 = Math.Sin(b / 2.0);
            d = 2
                    * R
                    * Math.Asin(Math.Sqrt(sa2 * sa2 + Math.Cos(lat1)
                            * Math.Cos(lat2) * sb2 * sb2));
            return d;
        }

        static int getMenuChoice()
        {
            int menu;
            try
            {
                Console.Write("1. Add Airport\n" +
                            "2. List all airports\n" +
                            "3. Find nearest airport by code\n" +
                            "4. Find airport's elevation standard deviation\n" +
                            "5. Change log delegates\n" +
                            "0. Exit\n" +
                            "Enter your choice: ");
                //string strMenu = Console.ReadLine();
                menu = int.Parse(Console.ReadLine());

            }
            catch (FormatException ex)
            {
                menu = -1;
            }
            return menu;
        }

        static void AddAirport()
        {
            Console.WriteLine("");
            Console.Write("Please enter code:");
            string code = Console.ReadLine();
            Console.Write("Please enter your city:");
            string city = Console.ReadLine();

            double latitude;
            while (true)
            {
                Console.Write("Please enter latitude:");
                if (!double.TryParse(Console.ReadLine(), out latitude))
                {
                    Console.Write("The latitude invalid.\n");
                    continue;
                }
                break;
            }
            double longitude;
            while (true)
            {
                Console.Write("Please enter longitude:");
                if (!double.TryParse(Console.ReadLine(), out longitude))
                {
                    Console.Write("The longitude invalid.\n");
                    continue;
                }
                break;
            }

            int elevation;
            while (true)
            {
                Console.Write("Please enter elevation:");
                if (!int.TryParse(Console.ReadLine(), out elevation))
                {
                    Console.Write("The elevation invalid.\n");
                    continue;
                }
                break;
            }

            try
            {
                Airport ap = new Airport(code, city, latitude, longitude, elevation);
                AirportsList.Add(ap);
            }
            catch (InvalidDataException ex)
            {
                Console.Write(ex.Message);
            }
            Console.WriteLine("");
        }

        static void ListAirport()
        {
            Console.WriteLine("");
            foreach (Airport ap in AirportsList)
            {
                Console.WriteLine(ap);
            }
            Console.WriteLine("");
        }

        public static void LogToConsole(string msg)
        {
            Console.WriteLine("Event: " + msg);
        }

        public static void LogToFile(string msg)
        {
            try
            {
                File.AppendAllText(@"..\..\events.log", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + msg + "\r\n");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

        static void ReadDataFromFile()
        {
            try
            {
                string[] airports = File.ReadAllLines(filename);
                foreach (string line in airports)
                {
                    try
                    {
                        Airport ap = new Airport(line);
                        AirportsList.Add(ap);
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                //Console.WriteLine("Error reading file: " + ex.Message);
            }
        } // file not found is not an error

        static void WriteDataToFile()
        {
            try
            {
                string[] text = new string[AirportsList.Count];
                for (int i = 0; i < AirportsList.Count; i++)
                {
                    text[i] = AirportsList[i].ToDataString();
                }
                File.WriteAllLines(filename, text);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
    }
}
