﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Quiz2Passengers
{
    class DatabaseException : Exception
    {
        public DatabaseException(string msg, Exception innerException) : base(msg, innerException) { }
    }

    class Database
    {
        SqlConnection conn;


        public Database()
        {
            try
            {
                this.conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\trakadmin\Documents\dotnet\Quiz2Passengers\Passengers.mdf;Integrated Security=True;Connect Timeout=30");
                conn.Open();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Database connecting is error: " + ex.Message, ex);
            }
        }

        public void CloseConnecting()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        public void Delete(Passenger pasg)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM Passengers WHERE Id=@id", conn);
                //////////             

                command.Parameters.AddWithValue("@id", pasg.Id);
                command.ExecuteNonQuery();

                //////////
                command.Dispose();
            }catch(SqlException ex)
            {
                throw new DatabaseException("Deleting data error: "+ex.Message, ex);
            }
        }

        public void AddNew(Passenger pasg)
        {
           try
            {
                SqlCommand command = new SqlCommand("INSERT INTO Passengers(Name,PassportNo,Destination,DepartureDateTime) VALUES(@name,@passportNo,@destination,@departureDateTime)", conn);
                //////////             
                command.Parameters.AddWithValue("@name", pasg.Name);
                command.Parameters.AddWithValue("@passportNo", pasg.PassportNo);
                command.Parameters.AddWithValue("@destination", pasg.Destination);
                command.Parameters.AddWithValue("@departureDateTime", pasg.DepartureDateTime);
                command.ExecuteNonQuery();
                //////////
                command.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Adding data error: " + ex.Message, ex);
            }
        }

        public void UPdateData(Passenger pasg)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE Passengers SET Name=@name, PassportNo=@passportNo,Destination=@destination,DepartureDateTime=@departureDateTime WHERE Id=@id", conn);
                //////////             
                command.Parameters.AddWithValue("@name", pasg.Name);
                command.Parameters.AddWithValue("@passportNo", pasg.PassportNo);
                command.Parameters.AddWithValue("@destination", pasg.Destination);
                command.Parameters.AddWithValue("@departureDateTime", pasg.DepartureDateTime);
                command.Parameters.AddWithValue("@id", pasg.Id);
                command.ExecuteNonQuery();

                //////////
                command.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Updating data error: " + ex.Message, ex);
            }
        }

        public List<Passenger> ReadAllData()
        {
            List<Passenger> list = new List<Passenger>();
            try
            {
                SqlCommand comd = new SqlCommand("select * from Passengers", conn);
                SqlDataReader read = comd.ExecuteReader();
                while (read.Read())
                {
                    int id = read.GetInt32(read.GetOrdinal("Id"));
                    string name = read.GetString(read.GetOrdinal("Name"));
                    string passportNo = read.GetString(read.GetOrdinal("PassportNo"));
                    string destination = read.GetString(read.GetOrdinal("Destination"));
                    DateTime departureDateTime = read.GetDateTime(read.GetOrdinal("DepartureDateTime"));

                    Passenger pasg = new Passenger(){ Id = id, Name = name, PassportNo = passportNo, Destination = destination, DepartureDateTime = departureDateTime };
                    list.Add(pasg);
                }
                read.Close();
                comd.Dispose();
            }
            catch (SqlException ex)
            {
                throw new DatabaseException("Reading data error: " + ex.Message, ex);
            }
            return list;
        }
    }
}
