﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum SortOrderEnum { Name, PassNo, Dest, DeptDateTime }

        // PROBABLY NOT NEEDED: List<Passenger> passengerList;

        static public SortOrderEnum CurrSortOrder;

        public MainWindow()
        {
            InitializeComponent();

            refreshList();

        }

        private void refreshList()
        {
            // fetch all records from database
            // filter if user entered a word into Search text box, best done using LINQ again
            // order them by CurrSortOrder using LINQ
            // give result to ListView as ItemsSource

            List<Passenger> passengerList = Globals.Db.ReadAllData();
            string search = tbSearch.Text;
            if (search != "")
            {
                var querySearch = from passengers in passengerList
                                           where (passengers.Name.IndexOf(search, StringComparison.OrdinalIgnoreCase) != -1) || (passengers.Destination.IndexOf(search, StringComparison.OrdinalIgnoreCase) != -1)
                                  select passengers;
                passengerList = querySearch.ToList();
            }


            switch (CurrSortOrder){
                case SortOrderEnum.PassNo:
                    passengerList = passengerList.OrderBy(a => a.PassportNo).ToList();
                    break;
                case SortOrderEnum.Dest:
                    passengerList = passengerList.OrderBy(a => a.Destination).ToList();
                    break;
                case SortOrderEnum.DeptDateTime:
                    passengerList = passengerList.OrderBy(a => a.DepartureDateTime).ToList();
                    break;
                default:
                    passengerList = passengerList.OrderBy(a => a.Name).ToList();
                    break;
            }

            lvDataBinding.ItemsSource = passengerList;

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(null);
            if (dialog.ShowDialog() == true)
            {
                if (dialog.passenger != null)
                {
                    if (!dialog.IsEdit)
                    {
                        try
                        {
                            Globals.Db.AddNew(dialog.passenger);
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database loading data error\n" + ex.Message, "Database error");
                            return;
                        }
                    }
                    
                }
            }
            refreshList();
        }

        private void lvDataBinding_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog((Passenger)lvDataBinding.SelectedItem);
            if (dialog.ShowDialog() == true)
            {
                if (dialog.passenger != null)
                {
                    if (!dialog.IsEdit)
                    {
                        try
                        {
                            Globals.Db.AddNew(dialog.passenger);
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database adding data error\n" + ex.Message, "Database error");
                        }

                    }
                    else
                    {
                        try
                        {
                            Globals.Db.UPdateData(dialog.passenger);
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database updating data error\n" + ex.Message, "Database error");
                        }
                    }
                    refreshList();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Globals.Db.CloseConnecting();
        }

        private void btSoryby_Click(object sender, RoutedEventArgs e)
        {
            SortBy dialog = new SortBy();
            if (dialog.ShowDialog() == true)
            {
                refreshList();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            refreshList();
        }
    }
}
