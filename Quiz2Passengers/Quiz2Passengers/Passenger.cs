﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    public class Passenger
    {
        int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        string _name;
        public string Name {

            get
            {
                return _name;
            }

            set
            {
                if (value.Length<1 || value.Length>100)
                {
                    throw new ArgumentException("The name must be 1-100 letters.");
                }
                _name = value;
            }
        } // VC(100) NOT NULL

        string _passportno;
        public string PassportNo
        {

            get
            {
                return _passportno;
            }

            set
            {
                Regex rgx = new Regex(@"[A-Z]{2}[0-9]{6}");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The passport must be AB123456 format. ");
                }
                _passportno = value;
            }
        } // VC(10) NOT NULL

        string _destination;
        public string Destination
        {

            get
            {
                return _destination;
            }

            set
            {
                if (value.Length < 1 || value.Length > 100)
                {
                    throw new ArgumentException("The destination must be 1-100 letters.");
                }
                _destination = value;
            }
        } // VC(100) NOT NULL

        DateTime _departuredatetime;

        public DateTime DepartureDateTime
        {
            get
            {
                return _departuredatetime;
            }

            set
            {
                _departuredatetime = value;
            }
        }// DateTime type, visible as two inputs in UI

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", Name, PassportNo, Destination, DepartureDateTime.ToString("yyyy-MM-dd HH:mm"));
        }
    }
}
