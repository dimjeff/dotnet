﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for SortBy.xaml
    /// </summary>
    public partial class SortBy : Window
    {
        public SortBy()
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;

            string strSory = MainWindow.CurrSortOrder.ToString();

            switch (strSory)
            {
                case "PassNo":
                    radioPassNo.IsChecked = true;
                    break;
                case "Dest":
                    radioDestna.IsChecked = true;
                    break;
                case "DeptDateTime":
                    radioDeptDate.IsChecked = true;
                    break;
                default:
                    radioName.IsChecked = true;
                    break;
            }
        }

        private void SortbyRadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            string strSory = rb.Content.ToString();
            if (!Enum.TryParse(strSory, out MainWindow.SortOrderEnum soryType))
            {
                MessageBox.Show(this, "The sory by must be selected.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow.CurrSortOrder = soryType;
            DialogResult = true;
        }
    }
}
